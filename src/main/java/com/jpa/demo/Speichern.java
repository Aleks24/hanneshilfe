package com.jpa.demo;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Controller;

@Controller
public class Speichern {

    Sender sender;

    public Speichern(Sender sender) {
        this.sender = sender;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void liedspeichern(){
        Lied lied = new Lied("Test","pop","Test","2:30",12);

        sender.gsonSenden(lied);
    }
}
