package com.jpa.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Lied {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String saenger;
    private String genre;
    private String titel;
    private String laenge;
    private double preis;


    public Lied(String saenger, String genre, String titel, String laenge, double preis) {
        this.saenger = saenger;
        this.genre = genre;
        this.titel = titel;
        this.laenge = laenge;
        this.preis = preis;
    }

    public void setSänger(String sänger) {
        this.saenger = sänger;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public void setLaenge(String laenge) {
        this.laenge = laenge;
    }

    public void setPreis(double preis) {
        this.preis = preis;
    }

    public String getSänger() {
        return saenger;
    }

    public String getGenre() {
        return genre;
    }

    public String getTitel() {
        return titel;
    }

    public String getLaenge() {
        return laenge;
    }

    public double getPreis() {
        return preis;
    }

    @Override
    public String toString() {
        return "Lied{" +
                "sänger='" + saenger + '\'' +
                ", genre='" + genre + '\'' +
                ", titel='" + titel + '\'' +
                ", laenge='" + laenge + '\'' +
                ", preis='" + preis + '\'' +
                '}';
    }
}
