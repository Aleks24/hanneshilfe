package com.jpa.demo;

import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

@Service
public class Sender {

    private String gsonString;
    Gson gson = new Gson();

    LiedRepository liedRepository;


    public Sender(LiedRepository liedRepository) {
        this.liedRepository = liedRepository;
    }

    public void gsonSenden(Lied lied) {


        liedRepository.save(lied);

        /*
        this.gsonString = gson.toJson(lied);
        try {

            URL url = new URL("http://10.211.55.3/Restfull/Lied.php?lied=" + gsonString);
            URLConnection conn = url.openConnection();

            BufferedReader rd;
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;

            while ((line = rd.readLine()) != null) {

                System.out.println(line);
            }

            rd.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
*/
    }
/*
    public void toXml(Lied lied) {

        try {

            File file = new File("C:\\file.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(Lied.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();


            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(lied, file);
            jaxbMarshaller.marshal(lied, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }

*/
}
